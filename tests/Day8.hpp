#pragma once

#include <catch2/catch.hpp>

#include "../src/Day8.hpp"

#include <utility>

static constexpr auto DAY8 = "[Day 8]";

using namespace Catch::Matchers;

namespace Day8
{
// NOLINTNEXTLINE
TEST_CASE( "Given the initial value of the accumulator is 0; immediately "
           "before any instruction is executed a second time, what "
           "value is in the accumulator?",
           DAY8 )
{
  GIVEN( "A program that contains an infinite loop somewhere" )
  {
    AND_GIVEN( "A machine able to execute this program" )
    {
      machine m{ input };

      WHEN( "I ask the machine to execute the program" )
      {
        m.execute();

        AND_WHEN( "I query for the value of the accumulator just before any "
                  "instruction is executed a second time" )
        {
          auto accumulator = m.accumulator();

          THEN( "The accumulator value is as expected" )
          {
            REQUIRE( accumulator == 1548 );
          }
        }
      }
    }
  }
}

TEST_CASE(
  "Executing a 'nop' mnemonic should not modify the accumulator of the "
  "machine whatever its argument value",
  DAY8 )
{
  using D = input_t;

  auto programs = { D{ "nop +0" }, D{ "nop +1" }, D{ "nop -1" },
                    D{ "nop -12345" }, D{ "nop +12345" } };

  for( auto program : programs )
  {
    machine m{ program };
    const auto reference{ m };

    m.execute();

    REQUIRE( m.accumulator() == reference.accumulator() );
  }
}

TEST_CASE( "Executing an 'acc' mnemonic with '+0' as its argument should not "
           "modify the accumulator of the machine",
           DAY8 )
{
  machine m{ { "acc +0" } };

  m.execute();

  REQUIRE( m.accumulator() == 0 );
}

struct TestData
{
  input_t input;
  int expected;
};

TEST_CASE( "Executing an 'acc' mnemonic with a positive or negative value as "
           "argument should modify the value of the accumulator accordingly",
           DAY8 )
{
  using D = TestData;

  auto data = { D{ .input{ "acc +0" }, .expected = 0 },
                D{ .input{ "acc +1" }, .expected = 1 },
                // NOLINTNEXTLINE
                D{ .input{ "acc +9" }, .expected = 9 },
                D{ .input{ "acc -1" }, .expected = -1 },
                // NOLINTNEXTLINE
                D{ .input{ "acc -9" }, .expected = -9 } };

  for( auto datum : data )
  {
    machine m{ datum.input };

    m.execute();

    REQUIRE( m.accumulator() == datum.expected );
  }
}

TEST_CASE( "A program of several lines made of 'acc' and 'nop' mnemonics "
           "compute the correct value for the machine's accumulator",
           DAY8 )
{
  input_t program{ "nop +123", "acc +1", "nop -456", "acc +2",
                   "nop +789", "acc +3", "nop -123", "acc +4",
                   "nop +456", "acc +5", "nop -789" };

  machine m{ program };

  m.execute();

  REQUIRE( m.accumulator() == 15 );
}

TEST_CASE( "A program containing a 'jmp' mnemonic does a jump from the current "
           "instruction relative to the argument of the 'jmp' mnemonic",
           DAY8 )
{
  using D = TestData;

  auto data = { D{ .input{ "acc +1", "jmp +1", "acc +2" }, .expected = 3 },
                D{ .input{ "acc +1", "jmp +2", "acc +2", "acc +3" },
                   .expected = 4 } };

  for( auto datum : data )
  {
    machine m{ datum.input };

    m.execute();

    REQUIRE( m.accumulator() == datum.expected );
  }
}

// Part 2
// NOLINTNEXTLINE
TEST_CASE( "Fix the program so that it terminates normally by changing exactly "
           "one jmp (to nop) or nop (to jmp). What is the value of the "
           "accumulator after the program terminates?",
           DAY8 )
{
  GIVEN( "A program that does not terminate" )
  {
    AND_GIVEN( "A machine initialized to run that program" )
    {
      machine m{ input };

      WHEN( "I repair that corrupted program that does not terminate" )
      {
        m.repair();

        AND_WHEN( "I execute the repaired program" )
        {
          m.execute();

          THEN( "The accumulator value is correct" )
          {
            REQUIRE( m.accumulator() == 1375 );
          }
        }
      }
    }
  }
}

// NOLINTNEXTLINE
TEST_CASE( "A machine initialized with a program can detect if the program is "
           "corrupted or not.",
           DAY8 )
{
  GIVEN( "A corrupted program" )
  {
    input_t corrupted{ "jmp +0" };

    AND_GIVEN( "A not corrupted program" )
    {
      input_t not_corrupted{ "jmp +1", "nop +42" };

      AND_GIVEN( "machines initialized with these programs" )
      {
        machine m_corrupted{ corrupted };
        machine m_not_corrupted{ not_corrupted };

        WHEN( "I query if the program is corrupted for those 2 machines" )
        {
          auto should_be_corrupted = m_corrupted.corrupted();
          auto should_not_be_corrupted = m_not_corrupted.corrupted();

          THEN( "The machine report the corrupted status of their respective "
                "program" )
          {
            REQUIRE( should_be_corrupted );
            REQUIRE( !should_not_be_corrupted );
          }
        }
      }
    }
  }
}
} // namespace Day8