#pragma once

#include <catch2/catch.hpp>

#include "../src/Day7.hpp"

#include <string>
#include <string_view>

using namespace Catch::Matchers;

static constexpr auto DAY7 = "[Day 7]";

static auto constexpr example_day7 =
  R"_(light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.)_";

static constexpr auto other_example =
  R"_(dark blue bags contain 2 dark violet bags.
dark yellow bags contain 2 dark green bags.
dark orange bags contain 2 dark yellow bags.
dark green bags contain 2 dark blue bags.
shiny gold bags contain 2 dark red bags.
dark violet bags contain no other bags.
dark red bags contain 2 dark orange bags.)_";

namespace Day7
{
TEST_CASE(
  "One line can be divided in 2 parts, the first one containing the color of "
  "the outer bag, the second one containing colors of contained bags.",
  DAY7 )
{
  using D = struct TestData
  {
    std::string_view input;
    struct expected
    {
      std::string outter_bag_color_info, inner_bags_colors_info;
    } expected;
  };

  GIVEN( "A possible input line containing outer and inner bag color infos" )
  {
    auto data = GENERATE(
      D{ .input{ "bright white bags contain 1 shiny gold bag." },
         .expected{ .outter_bag_color_info{ "bright white" },
                    .inner_bags_colors_info{ "1 shiny gold bag." } } },
      D{ .input{
           "light red bags contain 1 bright white bag, 2 muted yellow bags." },
         .expected{ .outter_bag_color_info{ "light red" },
                    .inner_bags_colors_info{
                      "1 bright white bag, 2 muted yellow bags." } } } );

    WHEN( "I extract bags colors info" )
    {
      bags_colors_info result = extract_bags_colors_info( data.input );

      THEN( "I can distinctly access to the outer bag color info and inner "
            "bags colors info." )
      {
        REQUIRE_THAT( std::string{ result.outer_bag },
                      Equals( data.expected.outter_bag_color_info ) );
        REQUIRE_THAT( std::string{ result.inner_bags },
                      Equals( data.expected.inner_bags_colors_info ) );
      }
    }
  }
}

// NOLINTNEXTLINE
TEST_CASE( "Precisely reporting the number of occurence a bag color appear "
           "when contained in an outter bag.",
           DAY7 )
{
  GIVEN( "A line of input" )
  {
    WHEN( "I query for an inner bag color I can find" )
    {
      auto count_existing =
        get_contained_bag_count_from_color( example_day7, "shiny gold bag" );

      AND_WHEN( "I query for an inner bag color I cannot find" )
      {
        auto count_unexisting = get_contained_bag_count_from_color(
          example_day7, "dotted white bag" );

        THEN( "The color I can find is reported the correct amount of time" )
        {
          REQUIRE( count_existing == 4 );

          AND_THEN( "The color I cannot find is reported 0 time" )
          {
            REQUIRE( count_unexisting == 0 );
          }
        }
      }
    }
  }
}

TEST_CASE(
  "Multi line input is split within a container, one element per line.", DAY7 )
{
  using D = struct TestData
  {
    std::string_view input;
    std::vector< std::string_view > expected;
  };

  auto data = GENERATE( D{ .input{ "abcdef" }, .expected{ "abcdef" } },
                        D{ .input{ "abc\ndef" }, .expected{ "abc", "def" } } );

  auto lines = split_line( data.input );

  REQUIRE_THAT( lines, UnorderedEquals( data.expected ) );
}

TEST_CASE( "Resolve Day 7 part 1", DAY7 )
{
  auto count = get_contained_bag_count_from_color( input, "shiny gold" );

  REQUIRE( count == 197 );
}

TEST_CASE( "Inner bag info should be split in as many part there are bags.",
           DAY7 )
{
  using D = struct
  {
    std::string_view input;
    bags_colors_info_2 expected;
  };

  auto data = GENERATE(
    D{ .input{ "bright white bags contain 3 shiny gold bag." },
       .expected{
         .outer_bag_color{ "bright white" },
         .inner_bag_color_info{ { .color{ "shiny gold" }, .count = 3 } } } },
    D{ .input{ "faded blue bags contain no other bags." },
       .expected{ .outer_bag_color{ "faded blue" } } },
    D{ .input{
         "dark orange bags contain 3 bright white bags, 4 muted yellow bags." },
       .expected{
         .outer_bag_color{ "dark orange" },
         .inner_bag_color_info{ { .color{ "bright white" }, .count = 3 },
                                { .color{ "muted yellow" }, .count = 4 } } } },
    D{ .input{ "dull silver bags contain 2 striped magenta bags, 2 dark coral "
               "bags, 1 bright orange bag, 4 plaid blue bags." },
       .expected{
         .outer_bag_color{ "dull silver" },
         .inner_bag_color_info{ { .color{ "striped magenta" }, .count = 2 },
                                { .color{ "dark coral" }, .count = 2 },
                                { .color{ "bright orange bag" }, .count = 1 },
                                { .color{ "plaid blue" }, .count = 4 } } } } );

  auto info = extract_bags_colors_info_2( data.input );

  REQUIRE_THAT( std::string{ info.outer_bag_color },
                Equals( std::string{ data.expected.outer_bag_color } ) );
  REQUIRE( info.inner_bag_color_info.size() ==
           data.expected.inner_bag_color_info.size() );

  if( data.expected.inner_bag_color_info.empty() )
    REQUIRE( info.inner_bag_color_info.empty() );
  else
  {
    REQUIRE_THAT(
      std::string{ info.inner_bag_color_info[ 0 ].color },
      Equals( std::string{ data.expected.inner_bag_color_info[ 0 ].color } ) );
    REQUIRE( info.inner_bag_color_info[ 0 ].count ==
             data.expected.inner_bag_color_info[ 0 ].count );
  }
}

TEST_CASE(
  "Inner bag info containing several bags should be split conveniently", DAY7 )
{
  constexpr auto data = "3 bright white bags, 4 muted yellow bags.";

  std::vector< bags_colors_info_2::inner_bag_color_info_t > info{};

  extract_inner_bag_info( data, info );

  REQUIRE( info.size() == 2 );

  REQUIRE( info[ 0 ].count == 3 );
  REQUIRE_THAT( std::string{ info[ 0 ].color }, Equals( "bright white" ) );
  REQUIRE( info[ 1 ].count == 4 );
  REQUIRE_THAT( std::string{ info[ 1 ].color }, Equals( "muted yellow" ) );
}

TEST_CASE(
  "Inner bag info can be summed to give the total amount of contained bags.",
  DAY7 )
{
  std::vector< bags_colors_info_2::inner_bag_color_info_t > info{
    { .count = 1 }, { .count = 2 }, { .count = 3 }, { .count = 4 }
  };

  auto count = total_inner_bag( info );

  REQUIRE( count == 10 );
}

TEST_CASE( "Inner bag info count can be multiplied by a factor.", DAY7 )
{
  std::vector< bags_colors_info_2::inner_bag_color_info_t > info{
    { .count = 1 }, { .count = 2 }, { .count = 3 }, { .count = 4 }
  };

  multiply_info_by( info, 3 );
  auto count = total_inner_bag( info );

  REQUIRE( count == 30 );
}

TEST_CASE( "Resolve Day 7 Part 2", DAY7 )
{
  using D = struct TestData
  {
    struct
    {
      std::string_view multi_line;
      std::string_view color;
    } input;
    std::size_t expected;
  };

  GIVEN( "A specified input" )
  {
    auto data = GENERATE(
      D{ .input{ .multi_line{ "faded blue bags contain no other bags." },
                 .color{ "bright white" } },
         .expected = 0 },
      D{ .input{ .multi_line{ "faded blue bags contain no other bags." },
                 .color{ "faded blue" } },
         .expected = 0 },
      D{ .input{ .multi_line{ R"_(faded blue bags contain 1 shiny gold bag.
shiny gold bags contain no other bags.)_" },
                 .color{ "faded blue" } },
         .expected = 1 },
      D{ .input{ .multi_line{ R"_(faded blue bags contain 2 shiny gold bag.
shiny gold bags contain no other bags.)_" },
                 .color{ "faded blue" } },
         .expected = 2 },
      D{ .input{
           .multi_line{
             R"_(faded blue bags contain 2 shiny gold bag, 4 plaid blue bags.
shiny gold bags contain no other bags.
plaid blue bags contain no other bags.)_" },
           .color{ "faded blue" } },
         .expected = 6 },
      D{ .input{ .multi_line{ R"_(faded blue bags contain 1 shiny gold bag.
shiny gold bags contain 1 blue bag.
blue bags contain no other bags.)_" },
                 .color{ "faded blue" } },
         .expected = 2 },
      D{ .input{ .multi_line{ example_day7 }, .color{ "shiny gold" } },
         .expected = 32 },
      D{ .input{ .multi_line{ other_example }, .color{ "shiny gold" } },
         .expected = 126 },
      D{ .input{
           .multi_line{
             R"_(faded blue bags contain 2 shiny gold bag, 4 plaid blue bags.
shiny gold bags contain 3 yellow bags, 3 brown bags.
plaid blue bags contain 5 brown bags bags.
brown bags contain no other bags.
yellow bags contain no other bags)_" },
           .color{ "faded blue" } },
         .expected = 38 },
      D{ .input{ .multi_line{ input }, .color{ "shiny gold" } },
         .expected = 85324 } );

    WHEN( "I compute the total amount of bags of a specified color" )
    {
      auto count = total_bag_count( data.input.multi_line, data.input.color );

      THEN( "I get the correct amount of bag" )
      {
        REQUIRE( count == data.expected );
      }
    }
  }
}
} // namespace Day7