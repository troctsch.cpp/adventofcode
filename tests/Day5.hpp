#pragma once

#include "catch2/catch.hpp"

#include "../src/Day5.hpp"

#include <algorithm>
#include <array>
#include <initializer_list>
#include <utility>

using namespace Catch::Matchers;

static constexpr auto DAY5 = "[Day 5]";

namespace Day5
{
TEST_CASE( "Mapping a string containing 'F' and 'B' characters should gives a "
           "string containing '0' or '1' characters, 'F' being mapped to '0' "
           "and 'B' being mapped to '1'",
           DAY5 )
{
  GIVEN( "The an input string" )
  {
    using D = struct TestData
    {
      std::string_view input, expected;
    };

    auto data = GENERATE(
      D{ .input = "F", .expected = "0" }, D{ .input = "B", .expected = "1" },
      D{ .input = "FF", .expected = "00" },
      D{ .input = "FBFBBFF", .expected = "0101100" },
      D{ .input = "L", .expected = "0" }, D{ .input = "R", .expected = "1" },
      D{ .input = "LL", .expected = "00" },
      D{ .input = "RLR", .expected = "101" } );

    WHEN( "I map this string" )
    {
      auto output = map( data.input );

      THEN( "I get the corresponding string made of 0 and 1 characters" )
      {
        REQUIRE_THAT( output, Equals( std::string{ data.expected } ) );
      }
    }
  }
}

TEST_CASE( "A string containing binary digits can be correctly converted to a "
           "positive integer",
           DAY5 )
{
  GIVEN( "A string of 0 and 1" )
  {
    using D = struct TestData
    {
      std::string_view input;
      unsigned int expected;
    };

    auto data = GENERATE( D{ .input = "0", .expected = 0 },
                          D{ .input = "1", .expected = 1 },
                          D{ .input = "0101100", .expected = 44 },
                          D{ .input = "101", .expected = 5 } );

    WHEN( "I convert this tring to an integer" )
    {
      auto value = convert( data.input );

      THEN( "I get the correct positive integer value" )
      {
        REQUIRE( value == data.expected );
      }
    }
  }
}

// NOLINTNEXTLINE
TEST_CASE( "A string made of one contiguous sequence of 'B' or 'F' "
           "followed by a contiguous sequence of 'L' and 'R' can be split in "
           "two different strings, one by contiguous sequences.",
           DAY5 )
{
  using D = struct TestData
  {
    std::string_view input;
    std::array< std::string_view, 2 > expected;
  };

  GIVEN( "A a string containing 2 contiguous sequences" )
  {
    auto data =
      GENERATE( D{ .input = "FL", .expected{ "F", "L" } },
                D{ .input = "BR", .expected{ "B", "R" } },
                D{ .input = "FBRL", .expected{ "FB", "RL" } },
                D{ .input = "FBFBBFFRLR", .expected{ "FBFBBFF", "RLR" } } );

    WHEN( "I partition this string" )
    {
      auto result = partition( data.input );

      THEN( "The partion is applied correctly" )
      {
        REQUIRE( result[ 0 ] == data.expected[ 0 ] );
        REQUIRE( result[ 1 ] == data.expected[ 1 ] );
      }
    }
  }
}

// NOLINTNEXTLINE
TEST_CASE( "A string made of one contiguous sequence of 'B' or 'F' "
           "followed by a contiguous sequence of 'L' and 'R' can be split in "
           "two different positive integers, one by contiguous sequences.",
           DAY5 )
{
  using D = struct TestData
  {
    std::string_view input;
    std::array< unsigned int, 2 > expected;
  };

  GIVEN( "A a string containing 2 contiguous sequences" )
  {
    auto data = GENERATE( D{ .input = "FL", .expected{ 0, 0 } },
                          D{ .input = "BR", .expected{ 1, 1 } },
                          D{ .input = "BFRL", .expected{ 2, 2 } },
                          D{ .input = "FBFBBFFRLR", .expected{ 44, 5 } } );

    WHEN( "I partition and convert this string" )
    {
      auto result = partition_convert( data.input );

      THEN( "The partion and the conversion is applied correctly" )
      {
        REQUIRE( result[ 0 ] == data.expected[ 0 ] );
        REQUIRE( result[ 1 ] == data.expected[ 1 ] );
      }
    }
  }
}

TEST_CASE(
  "A seat ID can be obtained from 2 positive integers and a given formula",
  DAY5 )
{
  using D = struct TestData
  {
    std::array< unsigned int, 2 > input;
    unsigned int expected;
  };

  GIVEN( "2 positive integers into an array" )
  {
    auto data = GENERATE( D{ .input{ 44, 5 }, .expected = 357 } );

    WHEN( "I compute the seat ID with thes numbers" )
    {
      auto seat_id = compute_seat_id( data.input );

      THEN( "I get the correct seat id" )
      {
        REQUIRE( seat_id == data.expected );
      }
    }
  }
}

// NOLINTNEXTLINE
TEST_CASE( "A sequence of seat 'names' gives a sequence of seat id sorted in "
           "descending order",
           DAY5 )
{
  GIVEN( "A sequence of seat names" )
  {
    WHEN( "I transform those names to seat ids" )
    {
      auto values = transform_and_sort( input );

      THEN( "I get an ordered sequence of matching seat ids" )
      {
        REQUIRE( values.size() == input.size() );
        REQUIRE( std::is_sorted( std::cbegin( values ), std::cend( values ),
                                 std::greater_equal{} ) );
      }
    }
  }
}

TEST_CASE( "In an integer sequence of seat id with each identifier being "
           "greater than its follower by 1, I can find a hole where one "
           "identifier is not followed by another one that is lesser by 1",
           DAY5 )
{
  GIVEN( "A sequence of seat identifier" )
  {
    auto values = transform_and_sort( input );

    WHEN( "I search for a missing identifier" )
    {
      auto id = missing_identifier( values );

      THEN( "I get the missing identifier" )
      {
        REQUIRE( id != values.front() );
      }
    }
  }
}
} // namespace Day5
