#pragma once

#include <catch2/catch.hpp>

#include "../src/Day1.hpp"

static constexpr auto DAY1 = "[Day 1]";

TEST_CASE( "Attempting to resolve the day 1 challenge should fail with no data",
           DAY1 )
{
  REQUIRE_THROWS_AS( Day1::resolve_day_1_challenge_part_1( {} ),
                     Day1::MissingDataException );
}

TEST_CASE(
  "Attempting to resolve the day 1 challenge should fail with bad data", DAY1 )
{
  REQUIRE_THROWS_AS( Day1::resolve_day_1_challenge_part_1( { 1 } ),
                     Day1::BadDataException );
}

TEST_CASE( "2 integers in a sequence summing to 2020 should exist", DAY1 )
{
  Day1::result1 r{};

  REQUIRE_NOTHROW(
    [ & ]() mutable
    { r = Day1::resolve_day_1_challenge_part_1( Day1::values ); }() );

  REQUIRE( r.first_value + r.second_value == 2020 );
}

TEST_CASE( "3 integers in a sequence summing to 2020 should exist", DAY1 )
{
  Day1::result2 r{};

  REQUIRE_NOTHROW(
    [ & ]() mutable
    { r = Day1::resolve_day_1_challenge_part_2( Day1::values ); }() );

  REQUIRE( r.first_value + r.second_value + r.third_value == 2020 );
}
