#pragma once

#include <catch2/catch.hpp>

#include "../src/Day2.hpp"

static constexpr auto DAY2 = "[Day 2]";

TEST_CASE( "There are several valid password in the input for the first policy",
           DAY2 )
{
  auto validPasswordCount = Day2::GetValidPasswordCountForFirstPolicy();

  REQUIRE( validPasswordCount > 0 );
}

TEST_CASE(
  "There are several valid password in the input for the second policy", DAY2 )
{
  auto validPasswordCount = Day2::GetValidPasswordCountForSecondPolicy();

  REQUIRE( validPasswordCount > 0 );
}