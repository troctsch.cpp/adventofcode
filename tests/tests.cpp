#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>

#include "Day1.hpp"
#include "Day10.hpp"
#include "Day2.hpp"
#include "Day3.hpp"
#include "Day4.hpp"
#include "Day5.hpp"
#include "Day6.hpp"
#include "Day7.hpp"
#include "Day8.hpp"
#include "Day9.hpp"