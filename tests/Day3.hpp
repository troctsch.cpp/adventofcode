#pragma once

#include <catch2/catch.hpp>

#include "../src/Day3.hpp"

static constexpr auto DAY3 = "[Day 3]";

TEST_CASE( "Traversal of any kind of puzzle gives the right amount of sharp "
           "character encoutered",
           DAY3 )
{
  GIVEN( "An example of puzzle" )
  {
    Day3::Puzzle puzzle{ R"_(..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#)_",
                         { .right = 3, .down = 1 } };

    WHEN( "I traverse this puzzle" )
    {
      std::size_t count = 0;
      auto traverse = [ & ]() { count = puzzle.traverse(); };

      THEN( "The traversal succeeds" )
      {
        REQUIRE_NOTHROW( traverse() );
        REQUIRE( count == 7 );
      }
    }
  }
}

TEST_CASE( "The final test case aiming to count exactly sharps as trees", DAY3 )
{
  GIVEN(
    "The provided puzzle for the challenge with its set of traversal rules" )
  {
    Day3::Puzzle puzzle{ Day3::puzzle, { .right = 3, .down = 1 } };

    WHEN( "I traverse this puzzle" )
    {
      std::size_t count = 0;
      auto traverse = [ & ]() { count = puzzle.traverse(); };

      THEN( "The traversal succeeds" )
      {
        REQUIRE_NOTHROW( traverse() );
        REQUIRE( count == 151 );
      }
    }
  }
}

// NOLINTNEXTLINE
TEST_CASE(
  "Weird computation of encoutered trees with several given traversal rules",
  DAY3 )
{
  GIVEN( "A set of traversal rules" )
  {
    auto traversal_rules = { Day3::TraversalRule{ .right = 1, .down = 1 },
                             Day3::TraversalRule{ .right = 3, .down = 1 },
                             // NOLINTNEXTLINE
                             Day3::TraversalRule{ .right = 5, .down = 1 },
                             // NOLINTNEXTLINE
                             Day3::TraversalRule{ .right = 7, .down = 1 },
                             Day3::TraversalRule{ .right = 1, .down = 2 } };

    AND_GIVEN( "An arbitrary puzzle input with an odd height greater than 1" )
    {
      auto input = R"_(.#
#.
.#)_";
      AND_GIVEN( "A puzzle built this input and this set of traversal rules" )
      {
        Day3::Puzzle puzzle{ Day3::puzzle, traversal_rules };

        WHEN( "I traverse this puzzle" )
        {
          std::size_t count = 0;
          auto traverse = [ & ]() mutable { count = puzzle.traverse(); };

          THEN( "The traversal succeeds" )
          {
            REQUIRE_NOTHROW( traverse() );
            REQUIRE( count >= 0 );
          }
        }
      }
    }
  }
}