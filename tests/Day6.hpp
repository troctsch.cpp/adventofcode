#pragma once

#include <catch2/catch.hpp>

#include "../src/Day6.hpp"

#include <initializer_list>
#include <utility>

using namespace Catch::Matchers;

static constexpr auto DAY6 = "[Day 6]";

namespace Day6
{
struct Part1TestData
{
  std::string_view input;
  std::size_t expected;
};

TEST_CASE( "A multi line input gives as many group there is line blocks "
           "separated by blank lines",
           DAY6 )
{
  GIVEN( "An example of multi line input" )
  {
    using D = Part1TestData;

    auto data = GENERATE( D{ .input = "", .expected = 0 },
                          D{ .input = "\n", .expected = 0 },
                          D{ .input = "\n\n", .expected = 0 },
                          D{ .input = "\n\n\n", .expected = 0 },
                          D{ .input = "foo\n\nbar", .expected = 2 },
                          D{ .input = "\n\nlol\n\nkek\n\n", .expected = 2 },
                          D{ .input = R"_(abc
def
ghi)_",
                             .expected = 1 },
                          D{ .input = "foo", .expected = 1 },
                          D{ .input = "bar\nbaz\nbee\nfee", .expected = 1 },
                          D{ .input = "\nbaby\nchild\n\n", .expected = 1 },
                          D{ .input = "\n\n\nlol", .expected = 1 } );

    WHEN( "I build groups from this example" )
    {
      groups g{ data.input };

      THEN( "I get the size of groups is as expected" )
      {
        REQUIRE( g.size() == data.expected );
      }
    }
  }
}

TEST_CASE(
  "A single line input create a group in which each letter is taken into "
  "account only one time.",
  DAY6 )
{
  using D = Part1TestData;

  GIVEN( "A one line input of letters" )
  {
    auto data = GENERATE( D{ .input = "a", .expected = 1 },
                          D{ .input = "ab", .expected = 2 },
                          D{ .input = "aab", .expected = 2 },
                          D{ .input = "abcabcabcbabc", .expected = 3 } );

    WHEN( "I build groups from that input" )
    {
      groups g{ data.input };

      THEN( "I can query the group within, having a size of the number of "
            "different letter used." )
      {
        REQUIRE( std::cbegin( g.sets() )->size() == data.expected );
      }
    }
  }
}

TEST_CASE( "Resolving Day 6 part 1", DAY6 )
{
  using D = Part1TestData;

  GIVEN( "An input" )
  {
    auto data = GENERATE( D{ .input = "abc\n\na", .expected = 4 },
                          D{ .input = R"_(abc

a
b
c

ab
ac

a
a
a
a

b)_",
                             .expected = 11 },
                          D{ .input = input, .expected = 6551 } );

    WHEN( "I build groups from this input" )
    {
      groups g{ data.input };

      THEN( "The puzzle is resolved" )
      {
        REQUIRE( g.compute_answers() == data.expected );
      }
    }
  }
}

TEST_CASE( "Resolves part 2", DAY6 )
{
  using D = struct TestData
  {
    std::string_view input;
    std::size_t expected;
  };

  GIVEN( "Multi line complete input" )
  {
    auto data = GENERATE( D{ .input = input, .expected = 3358 } );

    WHEN( "I build a group from that input" )
    {
      groups g{ data.input };

      THEN( "I get the solution of the puzzle" )
      {
        REQUIRE( g.sum_group_intersection_character_occurences() ==
                 data.expected );
      }
    }
  }
}

// TODO - implementation detail testing
TEST_CASE( "getting the least common string of a sequence of strings whichever "
           "their content returns an ordered sequence of common character in "
           "these strings",
           DAY6 )
{
  using D = struct TestData
  {
    std::initializer_list< std::string_view > input;
    std::size_t expected{};
  };

  std::initializer_list< D > data{
    D{ .input{}, .expected = 0 }, D{ .input{ "aof" }, .expected = 3 },
    D{ .input{ "abcdef", "def", "e" }, .expected = 1 },
    D{ .input{ "", "" }, .expected = 0 },
    D{ .input{ "", "", "" }, .expected = 0 },
    D{ .input{ "a", "" }, .expected = 0 },
    D{ .input{ "", "a" }, .expected = 0 },
    D{ .input{ "a", "", "" }, .expected = 0 },
    D{ .input{ "", "a", "" }, .expected = 0 },
    D{ .input{ "", "", "a" }, .expected = 0 },
    D{ .input{ "a", "a" }, .expected = 1 },
    D{ .input{ "a", "a", "a" }, .expected = 1 },
    D{ .input{ "b", "b" }, .expected = 1 },
    D{ .input{ "b", "b", "b" }, .expected = 1 },
    D{ .input{ "ab", "ab" }, .expected = 2 },
    D{ .input{ "ab", "ab", "ab" }, .expected = 2 },
    D{ .input{ "ab", "ba" }, .expected = 2 },
    D{ .input{ "ab", "ba", "ab", "ba" }, .expected = 2 },
    D{ .input{ "ab", "ac" }, .expected = 1 },
    D{ .input{ "ab", "ac", "ac", "ab" }, .expected = 1 },
    D{ .input{ "abx", "abc" }, .expected = 2 },
    D{ .input{ "abx", "abc", "aby" }, .expected = 2 },
    D{ .input{ "bxa", "abc", "aby" }, .expected = 2 },
    // NOLINTNEXTLINE
    D{ .input{ "azerty", "azerty", "azerty" }, .expected = 6 },
    D{ .input{ "ayrhfgcvlm", "abcrnvutyfghjk", "zerabyxcvbn", "yrca" },
       .expected = 4 },
    // NOLINTNEXTLINE
    D{ .input{ "dlevrzpucfmsq", "dpceruqsfmlv" }, .expected = 12 }
  };

  for( const D &d : data )
  {
    auto input = d.input;
    auto result = groups::least_common_string( input );

    REQUIRE( result == d.expected );
  }
}
} // namespace Day6