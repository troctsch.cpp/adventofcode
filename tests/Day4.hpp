#pragma once

#include <catch2/catch.hpp>

#include "../src/Day4.hpp"

using namespace Catch::Matchers;

static constexpr auto DAY4 = "[Day 4]";

static constexpr auto example_day4 =
  R"_(ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in



)_";

TEST_CASE( "The passport processor is able to recognize valid documents", DAY4 )
{
  GIVEN( "An input of documents" )
  {
    AND_GIVEN( "A passport processor fed with that input" )
    {
      Day4::PassportProcessor pp{ Day4::input };

      WHEN( "I query the number of valid documents" )
      {
        auto count = pp.valid_documents().size();

        THEN( "I get the correct number of valid documents" )
        {
          REQUIRE( count == 116 );
        }
      }
    }
  }
}

TEST_CASE( "The passport processor contains all documents specified as input",
           DAY4 )
{
  GIVEN( "An input of documents" )
  {
    AND_GIVEN( "A passport processor fed with that input" )
    {
      Day4::PassportProcessor pp{ example_day4 };

      WHEN( "I query the number of valid documents" )
      {
        auto count = pp.documents().size();

        THEN( "I get the correct number of valid documents" )
        {
          REQUIRE( count == 4 );
        }
      }
    }
  }
}

TEST_CASE( "A document is invalid if at least one of its mandatory field is "
           "not initialized",
           DAY4 )
{
  using D = Day4::Document;

  auto document = GENERATE( D{ "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd "
                               "byr:1937 iyr:2017" },
                            D{ "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd "
                               "byr:1937" },
                            D{ "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd" },
                            D{ "ecl:gry pid:860033327 eyr:2020" },
                            D{ "ecl:gry pid:860033327" }, D{ "" } );

  REQUIRE( !document.valid() );
}

TEST_CASE(
  "A document is invalid if mandatory fields are initialized with wrong values",
  DAY4 )
{
  Day4::Document document{ "ecl:~gry pid:~860033327 eyr:~2020 hcl:~#fffffd "
                           "byr:~1937 iyr:~2017 cid:~147 hgt:183cm" };

  REQUIRE( !document.valid() );
}

TEST_CASE( "Initializing a 'byr' field of a document with an incorrect value "
           "leaves the field uninitialized",
           DAY4 )
{
  auto input =
    // NOLINTNEXTLINE
    GENERATE( "byr:...invalid diabolical input for a fancy field",
              "byr:", "byr:1885", "byr:2050", "byr:2000abc", " byr:20001    " );

  Day4::Document document{ input };

  REQUIRE( document.byr().empty() );
}

using D = struct FieldTestData
{
  std::string_view input, expected;
};

TEST_CASE( "Initializing a 'byr' field of a document with a correct value "
           "intializes the field",
           DAY4 )
{
  auto data = GENERATE( D{ .input = "byr:1920", .expected = "1920" },
                        D{ .input = "byr:1921", .expected = "1921" },
                        D{ .input = "byr:1940", .expected = "1940" },
                        D{ .input = "byr:2000", .expected = "2000" },
                        D{ .input = "byr:2001", .expected = "2001" },
                        D{ .input = "byr:2002", .expected = "2002" } );

  Day4::Document document{ data.input.data() };

  REQUIRE_THAT( std::string{ document.byr() },
                Equals( std::string{ data.expected } ) );
}

TEST_CASE( "Initializing a 'ecl' field of a document with an incorrect value "
           "leaves the field uninitialized",
           DAY4 )
{
  // NOLINTNEXTLINE
  auto input = GENERATE( "ecl:...invalid diabolical input for a fancy field",
                         "ecl:", "ecl:1885", "ecl:2050", "ecl:othblu" );

  Day4::Document document{ input };

  REQUIRE( document.ecl().empty() );
}

TEST_CASE( "Initializing a 'ecl' field of a document with a correct value "
           "intializes the field",
           DAY4 )
{
  auto data = GENERATE( D{ .input = "ecl:amb", .expected = "amb" },
                        D{ .input = "ecl:blu", .expected = "blu" },
                        D{ .input = "ecl:brn", .expected = "brn" },
                        D{ .input = "ecl:gry", .expected = "gry" },
                        D{ .input = "ecl:grn", .expected = "grn" },
                        D{ .input = "ecl:hzl", .expected = "hzl" },
                        D{ .input = "ecl:oth", .expected = "oth" } );

  Day4::Document document{ data.input.data() };

  REQUIRE_THAT( std::string{ document.ecl() },
                Equals( std::string{ data.expected } ) );
}

TEST_CASE( "Initializing a 'eyr' field of a document with an incorrect value "
           "leaves the field uninitialized",
           DAY4 )
{
  // NOLINTNEXTLINE
  auto input = GENERATE( "eyr:...invalid diabolical input for a fancy field",
                         "eyr:", "eyr:1885", "eyr:2050", "eyr:zzz" );

  Day4::Document document{ input };

  REQUIRE( document.eyr().empty() );
}

TEST_CASE( "Initializing a 'eyr' field of a document with a correct value "
           "intializes the field",
           DAY4 )
{
  auto data = GENERATE( D{ .input = "eyr:2020", .expected = "2020" },
                        D{ .input = "eyr:2021", .expected = "2021" },
                        D{ .input = "eyr:2030", .expected = "2030" } );

  Day4::Document document{ data.input.data() };

  REQUIRE_THAT( std::string{ document.eyr() },
                Equals( std::string{ data.expected } ) );
}

TEST_CASE( "Initializing a 'hcl' field of a document with an incorrect value "
           "leaves the field uninitialized",
           DAY4 )
{
  // NOLINTNEXTLINE
  auto input = GENERATE( "hcl:...invalid diabolical input for a fancy field",
                         "hcl:", "hcl:#1885", "hcl:2050", "hcl:zzz" );

  Day4::Document document{ input };

  REQUIRE( document.hcl().empty() );
}

TEST_CASE( "Initializing a 'hcl' field of a document with a correct value "
           "intializes the field",
           DAY4 )
{
  auto data = GENERATE( D{ .input = "hcl:#02468a", .expected = "#02468a" },
                        D{ .input = "hcl:#ffffff", .expected = "#ffffff" },
                        D{ .input = "hcl:#badc0d", .expected = "#badc0d" } );

  Day4::Document document{ data.input.data() };

  REQUIRE_THAT( std::string{ document.hcl() },
                Equals( std::string{ data.expected } ) );
}

TEST_CASE( "Initializing a 'hgt' field of a document with an incorrect value "
           "leaves the field uninitialized",
           DAY4 )
{
  // NOLINTNEXTLINE
  auto input = GENERATE( "hgt:...invalid diabolical input for a fancy field",
                         "hgt:", "hgt:#1885", "hgt:2050", "hgt:zzz",
                         "hgt:149cm", "hgt:58in", "hgt:194cm" );

  Day4::Document document{ input };

  REQUIRE( document.hgt().empty() );
}

TEST_CASE( "Initializing a 'hgt' field of a document with a correct value "
           "intializes the field",
           DAY4 )
{
  auto data = GENERATE( D{ .input = "hgt:150cm", .expected = "150cm" },
                        D{ .input = "hgt:193cm", .expected = "193cm" },
                        D{ .input = "hgt:59in", .expected = "59in" },
                        D{ .input = "hgt:76in", .expected = "76in" } );

  Day4::Document document{ data.input.data() };

  REQUIRE_THAT( std::string{ document.hgt() },
                Equals( std::string{ data.expected } ) );
}

TEST_CASE( "Initializing a 'iyr' field of a document with an incorrect value "
           "leaves the field uninitialized",
           DAY4 )
{
  // NOLINTNEXTLINE
  auto input = GENERATE( "iyr:...invalid diabolical input for a fancy field",
                         "iyr:", "iyr:885", "iyr:2050", "iyr:zzz" );

  Day4::Document document{ input };

  REQUIRE( document.iyr().empty() );
}

TEST_CASE( "Initializing a 'iyr' field of a document with a correct value "
           "intializes the field",
           DAY4 )
{
  auto data = GENERATE( D{ .input = "iyr:2010", .expected = "2010" },
                        D{ .input = "iyr:2011", .expected = "2011" },
                        D{ .input = "iyr:2019", .expected = "2019" },
                        D{ .input = "iyr:2020", .expected = "2020" } );

  Day4::Document document{ data.input.data() };

  REQUIRE_THAT( std::string{ document.iyr() },
                Equals( std::string{ data.expected } ) );
}

TEST_CASE( "Initializing a 'pid' field of a document with an incorrect value "
           "leaves the field uninitialized",
           DAY4 )
{
  // NOLINTNEXTLINE
  auto input = GENERATE( "pid:...invalid diabolical input for a fancy field",
                         "pid:", "pid:885", "pid:2050", "pid:zzz" );

  Day4::Document document{ input };

  REQUIRE( document.pid().empty() );
}

TEST_CASE( "Initializing a 'pid' field of a document with a correct value "
           "intializes the field",
           DAY4 )
{
  auto data =
    GENERATE( D{ .input = "pid:000000000", .expected = "000000000" },
              D{ .input = "pid:010101010", .expected = "010101010" },
              D{ .input = "pid:123456789", .expected = "123456789" },
              D{ .input = "pid:900000000", .expected = "900000000" } );

  Day4::Document document{ data.input.data() };

  REQUIRE_THAT( std::string{ document.pid() },
                Equals( std::string{ data.expected } ) );
}
