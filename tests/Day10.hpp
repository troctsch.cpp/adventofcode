#pragma once

#include <catch2/catch.hpp>

#include "../src/Day10.hpp"

static constexpr auto DAY10 = "[Day 10]";

using namespace Catch::Matchers;

namespace Day10
{
TEST_CASE( "Using all adapters in a bag, they can be rearranged in sort of no "
           "one is more than 3 and less than 1 jolt from its predecessor.",
           DAY10 )
{
  // NOLINTNEXTLINE
  auto adapters = { 16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4 };

  serial_jolter s{ adapters };

  REQUIRE_THAT( s.adapters(), Equals( std::vector< int >{ 1, 4, 5, 6, 7, 10, 11,
                                                          12, 15, 16, 19 } ) );
}

TEST_CASE(
  "In a sorted sequence of adapters, a serial jolter should report the correct "
  "number of 1 jolt difference between all of those serialized adapters.",
  DAY10 )
{
  // NOLINTNEXTLINE
  auto adapters = { 16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4 };

  serial_jolter s{ adapters };

  REQUIRE( s.one_jolt_diff_count() == 7 );
}

TEST_CASE(
  "In a sorted sequence of adapters, a serial jolter should report the correct "
  "number of 3 jolts difference between all of those serialized adapters.",
  DAY10 )
{
  // NOLINTNEXTLINE
  auto adapters = { 16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4 };

  serial_jolter s{ adapters };

  REQUIRE( s.three_jolts_diff_count() == 5 );
}

TEST_CASE(
  "In a larger example, one and three jolts difference are correctly found.",
  DAY10 )
{
  // NOLINTNEXTLINE
  auto adapters = { 28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24,
                    // NOLINTNEXTLINE
                    23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35,
                    // NOLINTNEXTLINE
                    8, 17, 7, 9, 4, 2, 34, 10, 3 };

  serial_jolter s{ adapters };

  REQUIRE( s.one_jolt_diff_count() == 22 );
  REQUIRE( s.three_jolts_diff_count() == 10 );
}

TEST_CASE( "Resolve the Day 10 part 1 challenge", DAY10 )
{
  serial_jolter s{ input };

  REQUIRE( s.one_jolt_diff_count() == 71 );
  REQUIRE( s.three_jolts_diff_count() == 29 );

  REQUIRE( s.final_joltage() == 2059 );
}

TEST_CASE( "The correct number of arrangement of adapters can be found given "
           "previous rules",
           DAY10 )
{
  using D = struct test_data
  {
    std::initializer_list< int > input;
    std::size_t expected{};
  };

  // NOLINTNEXTLINE
  auto data = {
    // NOLINTNEXTLINE
    D{ .input{ 1, 2, 3, 5 }, .expected = 6 },
    D{ .input{ 1, 2, 4 }, .expected = 3 }, D{ .input{ 1, 3 }, .expected = 2 },
    D{ .input{ 1, 2 }, .expected = 2 },
    // NOLINTNEXTLINE
    D{ .input{ 4, 5, 6, 7 }, .expected = 4 },
    // NOLINTNEXTLINE
    D{ .input{ 10, 11, 12 }, .expected = 2 },
    // NOLINTNEXTLINE
    D{ .input{ 16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4 }, .expected = 8 },
    // NOLINTNEXTLINE
    D{ .input{ 1, 2, 3, 4 }, .expected = 7 },
    D{ .input{ 1, 2, 3 }, .expected = 4 }, D{ .input{ 1, 4 }, .expected = 1 },
    D{ .input{ 3 }, .expected = 1 }, D{ .input{ 2 }, .expected = 1 },
    D{ .input{ 1 }, .expected = 1 }
  };

  for( const auto &datum : data )
  {
    serial_jolter s{ datum.input };

    REQUIRE( s.freaking_combinatorics() == datum.expected );
  }
}
} // namespace Day10