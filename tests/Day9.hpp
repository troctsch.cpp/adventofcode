#pragma once

#include <catch2/catch.hpp>

#include "../src/Day9.hpp"

#include <utility>

static auto constexpr DAY9 = "[Day 9]";
// static auto DAY9_DISABLED = "[Day 9][.]";

namespace Day9
{
TEST_CASE( "Part 1 puzzle", DAY9 )
{
  GIVEN( "A packet of data provided by XMAS" )
  {
    xmas_input_t data = input;

    AND_GIVEN( "A XMAS decipher initialized with a preamble of 5" )
    {
      // NOLINTNEXTLINE
      xmas_decipher d{ 25 };

      WHEN( "Looking for a vulnerability in the packet of data" )
      {
        auto weakness = d.find_vulnerability( data );

        THEN( "The right vulnerability is found" )
        {
          REQUIRE( weakness == 27911108 );
        }
      }
    }
  }
}

TEST_CASE( "A xmas sequence with a specified preamble size should find the "
           "vulnerability",
           DAY9 )
{
  using D = struct TestData
  {
    struct
    {
      std::size_t preamble_size;
      xmas_input_t data;
    } input;

    unsigned long long expected;
  };

  auto data = {
    D{ .input{ .preamble_size = 2, .data{ 1, 2, 4 } }, .expected = 4 },
    // NOLINTNEXTLINE
    D{ .input{ .preamble_size = 2, .data{ 1, 2, 5 } }, .expected = 5 },
    // NOLINTNEXTLINE
    D{ .input{ .preamble_size = 2, .data{ 1, 2, 4, 6 } }, .expected = 4 },
    // NOLINTNEXTLINE
    D{ .input{ .preamble_size = 2, .data{ 1, 2, 3, 6 } }, .expected = 6 },
    // NOLINTNEXTLINE
    D{ .input{ .preamble_size = 2, .data{ 1, 2, 3, 5, 9 } }, .expected = 9 },
    // NOLINTNEXTLINE
    D{ .input{ .preamble_size = 2, .data{ 1, 2, 3, 5, 9, 14 } },
       // NOLINTNEXTLINE
       .expected = 9 },
    // NOLINTNEXTLINE
    D{ .input{ .preamble_size = 3, .data{ 1, 2, 3, 6 } }, .expected = 6 },
    // NOLINTNEXTLINE
    D{ .input{ .preamble_size = 3, .data{ 1, 2, 3, 4, 8 } }, .expected = 8 },
    // NOLINTNEXTLINE
    D{ .input{ .preamble_size = 5,
               // NOLINTNEXTLINE
               .data{ 35, 20, 15, 25, 47, 40, 62, 55, 65, 95,
                      // NOLINTNEXTLINE
                      102, 117, 150, 182, 127, 219, 299, 277, 309, 576 } },
       // NOLINTNEXTLINE
       .expected = 127 },
  };

  for( auto datum : data )
  {
    xmas_decipher d{ datum.input.preamble_size };

    auto v = d.find_vulnerability( datum.input.data );

    REQUIRE( v == datum.expected );
  }
}

TEST_CASE( "As the weakness has been found, find the sequence which contains "
           "numbers that add up to that weakness. Finally the result is the "
           "summ of the smallest and the biggest number",
           DAY9 )
{
  GIVEN( "A xmas decipher set up with a preamble size of 25" )
  {
    // NOLINTNEXTLINE
    xmas_decipher d{ 25 };

    AND_GIVEN( "The weakness found in the input" )
    {
      auto weakness = d.find_vulnerability( input );

      WHEN( "I compute the breach" )
      {
        auto breach = d.compute_breach( input, weakness );

        THEN( "The breach is found." )
        {
          REQUIRE( breach == 4023754 );
        }
      }
    }
  }
}
} // namespace Day9