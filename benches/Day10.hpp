#pragma once

#define CATCH_CONFIG_ENABLE_BENCHMARKING

#include <catch2/catch.hpp>

#include "../src/Day10.hpp"

static constexpr auto DAY10 = "[Day 10]";
static constexpr auto DAY10_DISABLED = "[Day 10][.]";

using namespace Catch::Matchers;

namespace Day10
{
TEST_CASE( "Benchmarking with optimization", DAY10_DISABLED )
{
  // NOLINTNEXTLINE
  auto data = { 28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38,
                // NOLINTNEXTLINE
                39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3 };

  BENCHMARK_ADVANCED(
    "Benchmarking sufficiently big input with an optimized algorithm" )
  ( Catch::Benchmark::Chronometer chrono )
  {
    serial_jolter s{ data };

    chrono.measure( [ &s ]() { return s.freaking_combinatorics(); } );
  };

  serial_jolter s{ data };

  REQUIRE( s.freaking_combinatorics() == 19208 );
}

TEST_CASE( "Resolve day 10 part 2", DAY10 )
{
  BENCHMARK_ADVANCED(
    "Benchmarking the puzzle input with an optimized algorithm" )
  ( Catch::Benchmark::Chronometer chrono )
  {
    serial_jolter s{ input };

    chrono.measure( [ &s ]() { return s.freaking_combinatorics(); } );
  };

  serial_jolter s{ input };

  REQUIRE( s.freaking_combinatorics() == 86812553324672 );
}
} // namespace Day10