#pragma once

#include "adventofcode_export.h"

#include <forward_list>
#include <string_view>
#include <vector>

namespace Day7
{
struct bags_colors_info
{
  std::string_view outer_bag, inner_bags;
};

bags_colors_info ADVENTOFCODE_EXPORT
extract_bags_colors_info( std::string_view line );

std::size_t ADVENTOFCODE_EXPORT get_contained_bag_count_from_color(
  std::string_view multi_line, std::string_view color );

std::string_view ADVENTOFCODE_EXPORT
get_containing_bag_color_from_color_for_line( std::string_view line,
                                              std::string_view color );

std::vector< std::string_view > ADVENTOFCODE_EXPORT
split_line( std::string_view line );

std::size_t ADVENTOFCODE_EXPORT get_contained_bag_count_from_expanding_colors(
  std::string_view multi_line,
  std::vector< std::string_view > &expanding_colors );

struct ADVENTOFCODE_EXPORT bags_colors_info_2
{
  struct inner_bag_color_info_t
  {
    std::string_view color;
    std::size_t count;
  };

  std::string_view outer_bag_color;
  std::vector< inner_bag_color_info_t > inner_bag_color_info;
};

bags_colors_info_2 ADVENTOFCODE_EXPORT
extract_bags_colors_info_2( std::string_view line );

void ADVENTOFCODE_EXPORT extract_inner_bag_info(
  std::string_view line,
  std::vector< bags_colors_info_2::inner_bag_color_info_t > &inner_info );

std::size_t ADVENTOFCODE_EXPORT total_inner_bag(
  const std::vector< bags_colors_info_2::inner_bag_color_info_t > &info );

void ADVENTOFCODE_EXPORT multiply_info_by(
  std::vector< bags_colors_info_2::inner_bag_color_info_t > &info,
  std::size_t factor );

std::size_t ADVENTOFCODE_EXPORT total_bag_count( std::string_view input,
                                                 std::string_view color );

bags_colors_info_2 color_info_from_lines_and_color(
  const std::vector< std::string_view > &lines, std::string_view color );

std::forward_list< bags_colors_info_2 > collect_and_multiply_all_bag_info(
  bags_colors_info_2 &&info, std::vector< std::string_view > &&lines );

extern const std::string_view input;
} // namespace Day7