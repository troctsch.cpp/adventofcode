#pragma once

#include "adventofcode_export.h"

#include <initializer_list>
#include <set>
#include <string_view>

namespace Day8
{
using input_t = std::initializer_list< std::string_view >;

class ADVENTOFCODE_EXPORT machine
{
public:
  machine( input_t program ) noexcept;
  void execute() noexcept;
  int accumulator() const noexcept;
  void repair() noexcept;
  bool corrupted() noexcept;

private:
  std::string_view pinpoint_next_instruction_to_repair() noexcept;
  std::string prepare_instruction_for_execution() noexcept;
  void clear_looping_instruction() noexcept;
  bool has_looping_instruction() const noexcept;

  input_t program_;
  int accumulator_{};
  std::set< std::ptrdiff_t > instruction_offsets_;
  decltype( program_.end() ) looping_instruction_;
  decltype( program_.begin() ) instruction_pointer_;
  decltype( program_.begin() ) next_instruction_to_repair_it_;
  bool repairing_{ false }, repaired_{ false };

  static int extract_mnemonic_argument( std::string_view instruction ) noexcept;
  static std::string_view extract_mnemonic_argument_as_string_view(
    std::string_view instruction ) noexcept;
  static bool is_patchable_instruction( std::string_view instruction ) noexcept;
  static bool is_jmp_instruction( std::string_view instruction ) noexcept;
  static std::string repair_instruction( std::string_view instruction );
};

extern const input_t input;
} // namespace Day8