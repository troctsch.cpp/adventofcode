#pragma once

#include "adventofcode_export.h"

#include <initializer_list>

namespace Day9
{
using xmas_input_t = std::initializer_list< unsigned long long >;

class ADVENTOFCODE_EXPORT xmas_decipher
{
public:
  explicit xmas_decipher( std::size_t preamble_size ) noexcept;
  unsigned long long find_vulnerability( xmas_input_t input ) noexcept;
  unsigned long long compute_breach( xmas_input_t input,
                                     unsigned long long weakness ) noexcept;

private:
  bool has_not_found_correct_sum_from(
    xmas_input_t::const_iterator first ) noexcept;
  bool has_found_breach( xmas_input_t::const_iterator it,
                         xmas_input_t::const_iterator end,
                         unsigned long long weakness ) noexcept;

  const std::size_t preamble_size_;
  xmas_input_t::const_iterator weakness_it_;
  unsigned long long breach_{};
};

extern const xmas_input_t input;
} // namespace Day9