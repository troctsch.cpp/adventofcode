#pragma once

#include "adventofcode_export.h"

#include <string_view>
#include <vector>

namespace Day3
{
struct TraversalRule
{
  std::uint8_t right, down;
};

class ADVENTOFCODE_EXPORT Puzzle
{
public:
  Puzzle( std::string_view s, TraversalRule rule );
  Puzzle( std::string_view s, std::initializer_list< TraversalRule > rules );

  std::size_t traverse() const;

private:
  std::size_t size() const;
  char at( std::size_t x, std::size_t y ) const;
  static bool isSharp( char c );
  std::size_t width() const;
  std::size_t height_limit() const;
  void init_lines( std::string_view s );
  std::size_t compute_result_for_rule( const TraversalRule &rule ) const;

  std::size_t input_size{};
  std::vector< std::string > lines;
  std::vector< TraversalRule > rules;
};

extern const char * const puzzle;
} // namespace Day3
