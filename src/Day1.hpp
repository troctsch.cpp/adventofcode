#pragma once

#include <exception>
#include <initializer_list>

#include "adventofcode_export.h"

namespace Day1
{
extern const std::initializer_list< int > values;

class MissingDataException : public std::exception
{
};

class BadDataException : public std::exception
{
};

struct result1
{
  int first_value{}, second_value{}, result{};
};

struct result2
{
  int first_value{}, second_value{}, third_value{}, result{};
};

ADVENTOFCODE_EXPORT result1
resolve_day_1_challenge_part_1( std::initializer_list< int > sequence );

ADVENTOFCODE_EXPORT result2
resolve_day_1_challenge_part_2( std::initializer_list< int > sequence );

} // namespace Day1
