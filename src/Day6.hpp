#pragma once

#include "adventofcode_export.h"

#include <set>
#include <string>
#include <string_view>
#include <vector>

namespace Day6
{
class ADVENTOFCODE_EXPORT groups
{
public:
  groups( std::string_view input );
  std::size_t size() const noexcept;
  bool empty() const noexcept;
  const std::vector< std::set< char > > &sets() const noexcept;
  std::size_t compute_answers() const;
  std::size_t sum_group_intersection_character_occurences();
  // TODO - move in implementation details
  static std::size_t least_common_string(
    std::vector< std::string_view > &&input );
  static std::string least_common_string_binary( std::string_view left,
                                                 std::string_view right );

private:
  static std::string_view trim_empty_lines( std::string_view input );

  void initialize_groups();
  void initialize_group( std::string_view::const_iterator &group_begin,
                         std::string_view::const_iterator end );
  void initialize_group_intersection_character_occurences(
    std::string_view::const_iterator &group_begin,
    std::string_view::const_iterator end );
  static std::vector< std::string_view > split( std::string_view s );

  std::string_view input_;
  std::vector< std::set< char > > sets_;
  std::vector< std::size_t > group_occurences_;
};

extern const std::string_view input;
} // namespace Day6