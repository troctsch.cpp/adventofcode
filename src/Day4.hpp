#pragma once

#include "adventofcode_export.h"

#include <optional>
#include <regex>
#include <string>
#include <string_view>
#include <vector>

namespace Day4
{
class ADVENTOFCODE_EXPORT Document
{
public:
  Document( const std::string &content );
  bool valid() const;
  std::string_view byr() const;
  std::string_view ecl() const;
  std::string_view eyr() const;
  std::string_view hcl() const;
  std::string_view hgt() const;
  std::string_view iyr() const;
  std::string_view pid() const;
  const std::optional< std::string_view > cid() const;

private:
  std::match_results< std::string::const_iterator > prevalidate_field(
    const std::string &pattern, const std::string &content );
  void initialize_byr_with( const std::string &content );
  void initialize_ecl_with( const std::string &content );
  void initialize_eyr_with( const std::string &content );
  void initialize_hcl_with( const std::string &content );
  void initialize_hgt_with( const std::string &content );
  void initialize_iyr_with( const std::string &content );
  void initialize_pid_with( const std::string &content );
  void initialize_cid_with( const std::string &content );

  std::string byr_, ecl_, eyr_, hcl_, hgt_, iyr_, pid_;
  std::optional< std::string > cid_;
};

class ADVENTOFCODE_EXPORT PassportProcessor
{
public:
  PassportProcessor( std::string_view input );
  std::vector< Document > valid_documents() const;
  const std::vector< Document > &documents() const;

private:
  static bool read_line( std::string_view &out_sv,
                         std::string_view::const_iterator &current,
                         std::string_view::const_iterator &end );
  void build_documents( std::string_view::const_iterator current,
                        std::string_view::const_iterator end );

  std::vector< Document > documents_;
};

extern const char * const input;
} // namespace Day4
