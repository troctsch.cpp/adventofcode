#pragma once

#include "adventofcode_export.h"

#include <array>
#include <initializer_list>
#include <string>
#include <string_view>
#include <vector>

namespace Day5
{
ADVENTOFCODE_EXPORT std::string map( std::string_view input );
ADVENTOFCODE_EXPORT unsigned int convert( std::string_view input );
ADVENTOFCODE_EXPORT std::array< std::string_view, 2 > partition(
  std::string_view input );
ADVENTOFCODE_EXPORT std::array< unsigned int, 2 > partition_convert(
  std::string_view input );
ADVENTOFCODE_EXPORT unsigned int compute_seat_id(
  std::array< unsigned int, 2 > input );
ADVENTOFCODE_EXPORT std::vector< unsigned int > transform_and_sort(
  std::initializer_list< std::string_view > input );
ADVENTOFCODE_EXPORT unsigned int missing_identifier(
  const std::vector< unsigned int > &values );

extern const std::initializer_list< std::string_view > input;
} // namespace Day5