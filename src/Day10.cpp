#include "Day10.hpp"

#include <algorithm>
#include <iterator>
#include <numeric>
#include <set>
#include <utility>

namespace Day10
{
serial_jolter::serial_jolter( std::initializer_list< int > input ) noexcept
{
  input_.reserve( input.size() );

  std::copy( input.begin(), input.end(), std::back_inserter( input_ ) );
  std::sort( input_.begin(), input_.end(), std::less{} );
}

const std::vector< int > &serial_jolter::adapters() const noexcept
{
  return input_;
}

std::size_t serial_jolter::one_jolt_diff_count() const noexcept
{
  return n_jolts_diff_count( 1 );
}

std::size_t serial_jolter::three_jolts_diff_count() const noexcept
{
  return n_jolts_diff_count( 3 ) + 1;
}

std::size_t serial_jolter::n_jolts_diff_count( int diff ) const noexcept
{
  int last_adapter{};
  std::size_t result{};

  std::for_each( input_.cbegin(), input_.cend(),
                 [ &result, &last_adapter, diff ]( auto adapter )
                 {
                   if( adapter - last_adapter == diff )
                     result++;

                   last_adapter = adapter;
                 } );

  return result;
}

std::size_t serial_jolter::final_joltage() const noexcept
{
  return one_jolt_diff_count() * three_jolts_diff_count();
}

void serial_jolter::insert_next_sequence( input_iterator begin,
                                          const input_iterator &it,
                                          input_iterator end,
                                          std::set< input_t > &sequences,
                                          int first_value )
{
  input_t seq{ std::move( begin ), it };
  std::copy( it + 1, std::move( end ), std::back_inserter( seq ) );

  if( valid_sequence( seq, first_value ) )
    if( !sequences.contains( seq ) )
      sequences.insert( std::move( seq ) );
}

bool serial_jolter::valid_sequence( const input_t &sequence,
                                    int first_value ) noexcept
{
  int current_val{ first_value };

  for( int val : sequence )
  {
    if( val - current_val > 3 )
      return false;

    current_val = val;
  }

  return true;
}

std::size_t serial_jolter::freaking_combinatorics() const
{
  if( input_.size() == 1 )
    return 1;

  auto segments = split_in_contiguous_value_segments();

  return std::accumulate( segments.cbegin(), segments.cend(), std::size_t{ 1U },
                          []( std::size_t result, auto segment ) {
                            return result *=
                                   segment_combination_count( segment );
                          } );
}

std::size_t serial_jolter::segment_combination_count(
  const std::tuple< serial_jolter::input_iterator,
                    serial_jolter::input_iterator > &segment )
{
  std::set< input_t > sequences{ { std::get< 0 >( segment ),
                                   std::get< 1 >( segment ) } };

  int first_value = *std::get< 0 >( segment ) - 3;
  if( first_value < 0 )
    first_value = 0;

  for( const auto &input : sequences )
    for( auto begin = input.cbegin(), it = begin, end = input.cend(),
              limit = end - 1;
         it != limit; ++it )
      insert_next_sequence( begin, it, end, sequences, first_value );

  return sequences.size();
}

std::forward_list<
  std::tuple< serial_jolter::input_iterator, serial_jolter::input_iterator > >
serial_jolter::split_in_contiguous_value_segments() const
{
  int initial = *input_.cbegin();

  std::forward_list< std::tuple< input_iterator, input_iterator > > segments;

  input_iterator begin;
  input_iterator it;
  input_iterator end;

  for( begin = input_.cbegin(), it = begin + 1, end = input_.cend(); it != end;
       ++it )
    if( *it - initial >= 3 )
    {
      segments.emplace_front( begin, it );
      initial = *it;
      begin = it;
    }
    else
      initial++;

  segments.emplace_front( begin, it );

  return segments;
}

const std::initializer_list< int > input = {
  77,  58,  25,  92,  14,  154, 105, 112, 147, 63,  84,  109, 24,  129, 49,
  102, 130, 128, 134, 88,  95,  70,  80,  4,   153, 17,  145, 122, 39,  117,
  93,  65,  3,   2,   139, 101, 148, 37,  27,  1,   87,  64,  23,  59,  42,
  146, 43,  151, 116, 46,  115, 118, 131, 94,  19,  33,  12,  107, 10,  7,
  73,  78,  53,  11,  135, 79,  60,  32,  141, 31,  140, 98,  136, 72,  38,
  152, 30,  74,  106, 50,  13,  26,  155, 67,  20,  66,  91,  56,  34,  125,
  52,  51,  18,  108, 57,  81,  119, 71,  144
};

} // namespace Day10
