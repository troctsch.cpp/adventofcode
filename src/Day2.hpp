#pragma once

#include "adventofcode_export.h"

#include <exception>
#include <initializer_list>

namespace Day2
{
extern const std::initializer_list< const char * > input;

class InvalidInputForPolicyExtractionException : public std::exception
{
};

class BadPolicyFormatException : public std::exception
{
};

class InvalidInputForPasswordExtractionException : public std::exception
{
};

class BadPasswordFormatException : public std::exception
{
};

ADVENTOFCODE_EXPORT std::size_t GetValidPasswordCountForFirstPolicy();
ADVENTOFCODE_EXPORT std::size_t GetValidPasswordCountForSecondPolicy();
} // namespace Day2