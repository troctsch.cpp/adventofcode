#pragma once

#include "adventofcode_export.h"

#include <forward_list>
#include <initializer_list>
#include <set>
#include <tuple>
#include <vector>

namespace Day10
{
class ADVENTOFCODE_EXPORT serial_jolter
{
public:
  using input_t = std::vector< int >;
  using input_iterator = input_t::const_iterator;

  serial_jolter( std::initializer_list< int > input ) noexcept;
  const input_t &adapters() const noexcept;
  std::size_t one_jolt_diff_count() const noexcept;
  std::size_t three_jolts_diff_count() const noexcept;
  std::size_t final_joltage() const noexcept;
  std::size_t freaking_combinatorics() const;

private:
  std::size_t n_jolts_diff_count( int diff ) const noexcept;
  std::size_t not_three_jolts_diff_count() const noexcept;
  std::forward_list< std::tuple< input_iterator, input_iterator > >
  split_in_contiguous_value_segments() const;

  static bool valid_sequence( const input_t &sequence,
                              int first_value ) noexcept;
  static std::size_t segment_combination_count(
    const std::tuple< input_iterator, input_iterator > &segment );
  static void insert_next_sequence( input_iterator begin,
                                    const input_iterator &it,
                                    input_iterator end,
                                    std::set< input_t > &sequences,
                                    int first_value );

  std::vector< int > input_;
};

extern const std::initializer_list< int > input;
} // namespace Day10